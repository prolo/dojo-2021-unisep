require_relative './main.rb'

describe 'Testando 576.73' do
    let(:valor) { 576.73 }
    let(:troco) {Troco.new(valor).troco() }
    
    it 'deve retornar 5 notas de cem reais' do
        expect(troco[100] == 5).to be_truthy
    end

    it 'deve retornar 1 nota de cinquenta reais' do
        expect(troco[50]== 1).to be_truthy
    end

    it 'deve retornar 1 nota de vinte reais' do
        expect(troco[20]== 1).to be_truthy
    end

    it 'deve retornar 1 nota de cinco reais' do
        expect(troco[5]== 1).to be_truthy
    end

    it 'deve retornar 1 moeda de um real' do
        expect(troco[1] == 1).to be_truthy
    end

    it 'deve retornar 1 moeda de cinquenta centavos' do
        expect(troco[0.5] == 1).to be_truthy
    end

    it 'deve retornar 2 moedas de dez centavos' do
        expect(troco[0.1] == 2).to be_truthy
    end

    it 'deve retornar 3 moedas de um centavo' do
        expect(troco[0.01] == 3).to be_truthy
    end
end

describe 'valor invalido' do
    it 'deve retornar exception quando o valor for negativo' do
        expect{Troco.new(rand * -1000).troco()}.to raise_error(RuntimeError)
    end

    it 'deve retornar exception quando o valor for maior que R$ 1000000.00' do
        expect{Troco.new(123123123124123124123).troco()}.to raise_error(RuntimeError)
    end
end