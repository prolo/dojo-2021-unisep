class Troco
    def initialize(valor)
        raise "Valor inválido" if valor < 0 || valor > 1000000.00
        @valor = valor
    end
        
    def troco
        contar_notas
    end

    private

    def contar_notas
        contagem_notas = {
            100 => 0,
            50 => 0,
            20 => 0,
            10 => 0,
            5 => 0,
            2 => 0
        }

        resto_troco = @valor.to_i
        contagem_notas.each do |valor_nota, _| 
            if resto_troco / valor_nota > 0
                contagem_notas[valor_nota] = resto_troco / valor_nota
                resto_troco %= valor_nota
            end
        end

        contagem_notas
    end

    def contar_moedas
        contagem_moedas = {
            1 => 0,
            0.5 => 0,
            0.25 => 0,
            0.1 => 0,
            0.05 => 0,
            0.01 => 0
        }

        resto_centavos = @valor - @valor.to_i
    
        contagem_moedas[1] = @valor.to_i % 10
        contagem_moedas[1] -= 5 if contagem_moedas[1] >= 5
        contagem_moedas[1] -= 2 if contagem_moedas[1] >= 2

        contagem_moedas.reject{ |moeda| moeda == 1 }.each do |valor_moeda, _| 
            if ((resto_centavos*100) / (valor_moeda*100)).to_i > 0
                contagem_moedas[valor_moeda] = ((resto_centavos*100) / (valor_moeda*100)).to_i
                resto_centavos -= contagem_moedas[valor_moeda] * valor_moeda
            end
        end

        contagem_moedas
    end

end

