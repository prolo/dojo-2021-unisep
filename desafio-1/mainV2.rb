class Troco
    def initialize(valor)
        raise "Valor inválido" if valor < 0 || valor > 1000000.00
        @valor = valor
    end
    def contar_troco
        contagem_notas_moedas = {       
            100 => 0,
            50 => 0,        
            20 => 0,       
            10 => 0,       
            5 => 0,      
            2 => 0,      
            1 => 0,     
            0.5 => 0,    
            0.25 => 0,   
            0.1 => 0, 
            0.05 => 0,  
            0.01 => 0    }
            
        contagem_notas_moedas.each do |valor_nota_moeda, _|                 
                contagem_notas_moedas[valor_nota_moeda] = (((resto_troco = @valor) - (@valor = @valor % valor_nota_moeda))/valor_nota_moeda)
        end
        contagem_notas_moedas
    end
end
Troco.new(576.73).contar_troco()
